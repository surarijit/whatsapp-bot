As the name suggests, my application is a Flask web application integrates the whatsapp business api with the backend.

Once an user sends a message from his whatsapp number to our twilio registration number, the response would be handled by our web application instead of Twilio.com 

My application works something like this

Our device running whatsapp <--> Whatsapp Server <--> Webhook <--> Our Web Application

Our web application would send a specific response to the message send by the user, we can ofcourse configure that.

For that we need to host this web application so that it is available anywhere around internet, and provide the URL for the website to the sandbox in our twilio sandbox account.